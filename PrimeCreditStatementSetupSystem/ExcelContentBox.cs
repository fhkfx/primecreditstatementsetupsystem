﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PrimeCreditStatementSetupSystem
{
    public partial class ExcelContentBox : UserControl
    {
        Dictionary<string, string> Dictionary_type = new Dictionary<string, string>();
        public ExcelContentBox()
        {
            InitializeComponent();
        }
        public class MyClass
        {
            public string Display { get; set; }
            public string Value { get; set; }
        }
        void checktextName(string element, string textName)
        {
            if (textName == "ID_TYPE") {
                Dictionary_type = new Dictionary<string, string>()
                {
                    {"B","BUSINESS REG."},
                    {"C","COMPANY NO."},
                    {"I","HKID CARD"},
                    {"P","PASSPORT"},
                    {"R","PRC ID"},
                    {"U","Unknown Individual"},
                    {"V","Unknown Company"},
                    {"X","DUMMY ID"},
                };
                checkkey(Dictionary_type, element);
            }
            else if (textName == "PROPERTY_TYPE_CODE")
            {
                Dictionary_type = new Dictionary<string, string>()
                {
                    {"DY","DORMITORY"},
                    {"HO","HOME OWNERSHIP SCHEME"},
                    {"OT","OTHERS"},
                    {"PE","PUBLIC ESTATE"},
                    {"PH","PRIVATE HOUSE"},
                    {"SC","SANDWICH CLASS HOUSING SCHEME"},
                    {"VH","VILLAGE HOUSE"},
                    {"blank"," "},
                };
                checkkey(Dictionary_type, element);
            }
            else if (textName == "PROPERTY_OWNER_CODE")
            {
                Dictionary_type = new Dictionary<string, string>()
                {
                    {"AF","APPLICANT & FAMILY"},
                    {"AL","ALONE"},
                    {"AP","APPLICANT"},
                    {"AS"," "},
                    {"BR","BROTHER"},
                    {"BS","BROTHER & SISTER"},
                    {"FA","FATHER"},
                    {"FF","FRIEND"},
                    {"FM","FAMILY"},
                    {"GF","GRANDFATHER"},
                    {"GM","GRANDMOTHER"},
                    {"MO","MOTHER"},
                    {"OT","OTHERS"},
                    {"PA","PARENT"},
                    {"S","APPLICANT & SPOUSE"},
                    {"SD","SON & DAUGHTER"},
                    {"SI","SISTER"},
                    {"SO","SON"},
                    {"SP","SPOUSE"},
                    {"blank"," "},
                };
                checkkey(Dictionary_type, element);
            }
            else if (textName == "PROPERTY_STATUS")
            {
                Dictionary_type = new Dictionary<string, string>()
                {
                    {"F","FULLY PAID"},
                    {"M","MORTGAGE"},
                    {"R","RENT"},
                    {"blank"," "},
                };
                checkkey(Dictionary_type, element);
            }
            else
            {
                checkedListBox1.Items.Add(new MyClass { Display = element + " Display", Value = element });
            }
            //checkedListBox1.Items.Insert(count, element);
        }
        public void checkkey(Dictionary<string, string> Dictionary_type, string element)
        {
            bool hit = false;
            foreach (KeyValuePair<string, string> ID_item in Dictionary_type)
                    {
                        if (element == ID_item.Key)
                        {
                            checkedListBox1.Items.Add(new MyClass { Display = ID_item.Value, Value = ID_item.Key
                            });
                            hit = true;
                        }
                    }
                    if(hit == false)
                    {
                        checkedListBox1.Items.Add(new MyClass { Display = element, Value = element });
                    }
        }
        public void setClb(string textName, string[] item, string type)
        {
            int count = 0;
            foreach (string element in item)
            {
                checktextName(element, textName);
                count++;
            }
            checkedListBox1.DisplayMember = "Display";
            checkedListBox1.ValueMember = "Value";
            BoxName.Text = textName;
            type_label.Text = type;
            this.Name = textName;
        }

        public void LoadClb(string textName, string[] item, string type, string[] clickitem)
        {
            int count = 0;
            foreach (string element in item)
            {
                checktextName(element, textName);
                foreach (string element2 in clickitem)
                {
                    if (element2 == element)
                    {
                        checkedListBox1.SetItemCheckState(count, CheckState.Checked);
                    }
                }
                count++;
            }
            checkedListBox1.DisplayMember = "Display";
            checkedListBox1.ValueMember = "Value";
            BoxName.Text = textName;
            type_label.Text = type;
            this.Name = textName;
        }

        public String getTextboxName()
        {
            return BoxName.Text;
        }

        public String getTextboxType()
        {
            return type_label.Text;
        }

        public List<string> getcheckedboxinfo()
        {
            var retList = new List<string>();
            foreach (Int32 item in checkedListBox1.CheckedIndices)
            {
                MyClass myclass = (MyClass)checkedListBox1.Items[item];
                retList.Add(myclass.Value.ToString());
            }
            return retList;
        }

        private void RemovelinkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            //this.Dispose();
        }
    }
}
