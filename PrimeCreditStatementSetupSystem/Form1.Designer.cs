﻿namespace PrimeCreditStatementSetupSystem
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.save_button = new System.Windows.Forms.Button();
            this.export_button = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.Browsebutton = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dw_comboBox = new System.Windows.Forms.ComboBox();
            this.add_dw_button = new System.Windows.Forms.Button();
            this.button15 = new System.Windows.Forms.Button();
            this.add_bi_button = new System.Windows.Forms.Button();
            this.bi_comboBox = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.clear_button = new System.Windows.Forms.Button();
            this.Load_button = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.R_numericUpDown = new System.Windows.Forms.NumericUpDown();
            this.MT_comboBox = new System.Windows.Forms.ComboBox();
            this.M_comboBox = new System.Windows.Forms.ComboBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.R_numericUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 16);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(819, 338);
            this.panel1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 81);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Statement";
            // 
            // save_button
            // 
            this.save_button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.save_button.Enabled = false;
            this.save_button.Location = new System.Drawing.Point(685, 598);
            this.save_button.Name = "save_button";
            this.save_button.Size = new System.Drawing.Size(75, 23);
            this.save_button.TabIndex = 3;
            this.save_button.Text = "Save";
            this.save_button.UseVisualStyleBackColor = true;
            this.save_button.Click += new System.EventHandler(this.Savebutton_Click);
            // 
            // export_button
            // 
            this.export_button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.export_button.Enabled = false;
            this.export_button.Location = new System.Drawing.Point(766, 598);
            this.export_button.Name = "export_button";
            this.export_button.Size = new System.Drawing.Size(75, 23);
            this.export_button.TabIndex = 4;
            this.export_button.Text = "Export";
            this.export_button.UseVisualStyleBackColor = true;
            this.export_button.Click += new System.EventHandler(this.export_button_Click);
            // 
            // textBox1
            // 
            this.textBox1.AccessibleDescription = "path_text";
            this.textBox1.Location = new System.Drawing.Point(133, 6);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(369, 20);
            this.textBox1.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(101, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Attributes Setup File";
            // 
            // Browsebutton
            // 
            this.Browsebutton.Location = new System.Drawing.Point(508, 4);
            this.Browsebutton.Name = "Browsebutton";
            this.Browsebutton.Size = new System.Drawing.Size(75, 23);
            this.Browsebutton.TabIndex = 9;
            this.Browsebutton.Text = "Browse";
            this.Browsebutton.UseVisualStyleBackColor = true;
            this.Browsebutton.Click += new System.EventHandler(this.Browse_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Location = new System.Drawing.Point(16, 235);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(825, 357);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Content";
            // 
            // dw_comboBox
            // 
            this.dw_comboBox.AccessibleDescription = "selectbox";
            this.dw_comboBox.AccessibleName = "";
            this.dw_comboBox.AllowDrop = true;
            this.dw_comboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.dw_comboBox.Enabled = false;
            this.dw_comboBox.FormattingEnabled = true;
            this.dw_comboBox.Location = new System.Drawing.Point(19, 50);
            this.dw_comboBox.Name = "dw_comboBox";
            this.dw_comboBox.Size = new System.Drawing.Size(203, 21);
            this.dw_comboBox.TabIndex = 11;
            // 
            // add_dw_button
            // 
            this.add_dw_button.Enabled = false;
            this.add_dw_button.Location = new System.Drawing.Point(228, 50);
            this.add_dw_button.Name = "add_dw_button";
            this.add_dw_button.Size = new System.Drawing.Size(75, 23);
            this.add_dw_button.TabIndex = 12;
            this.add_dw_button.Text = "Add";
            this.add_dw_button.UseVisualStyleBackColor = true;
            this.add_dw_button.Click += new System.EventHandler(this.add_dw_button_Click);
            // 
            // button15
            // 
            this.button15.Location = new System.Drawing.Point(935, 631);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(75, 23);
            this.button15.TabIndex = 13;
            this.button15.Text = "Clean";
            this.button15.UseVisualStyleBackColor = true;
            // 
            // add_bi_button
            // 
            this.add_bi_button.Enabled = false;
            this.add_bi_button.Location = new System.Drawing.Point(535, 52);
            this.add_bi_button.Name = "add_bi_button";
            this.add_bi_button.Size = new System.Drawing.Size(75, 23);
            this.add_bi_button.TabIndex = 15;
            this.add_bi_button.Text = "Add";
            this.add_bi_button.UseVisualStyleBackColor = true;
            this.add_bi_button.Click += new System.EventHandler(this.add_bi_button_Click);
            // 
            // bi_comboBox
            // 
            this.bi_comboBox.AccessibleDescription = "selectbox";
            this.bi_comboBox.AccessibleName = "";
            this.bi_comboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.bi_comboBox.Enabled = false;
            this.bi_comboBox.FormattingEnabled = true;
            this.bi_comboBox.Location = new System.Drawing.Point(326, 52);
            this.bi_comboBox.Name = "bi_comboBox";
            this.bi_comboBox.Size = new System.Drawing.Size(203, 21);
            this.bi_comboBox.TabIndex = 14;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 34);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(26, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "DW";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(323, 34);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(17, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "BI";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // clear_button
            // 
            this.clear_button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.clear_button.Enabled = false;
            this.clear_button.Location = new System.Drawing.Point(604, 598);
            this.clear_button.Name = "clear_button";
            this.clear_button.Size = new System.Drawing.Size(75, 23);
            this.clear_button.TabIndex = 3;
            this.clear_button.Text = "Clear";
            this.clear_button.UseVisualStyleBackColor = true;
            this.clear_button.Click += new System.EventHandler(this.clear_button_Click);
            // 
            // Load_button
            // 
            this.Load_button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Load_button.Enabled = false;
            this.Load_button.Location = new System.Drawing.Point(19, 598);
            this.Load_button.Name = "Load_button";
            this.Load_button.Size = new System.Drawing.Size(75, 23);
            this.Load_button.TabIndex = 16;
            this.Load_button.Text = "Load Save";
            this.Load_button.UseVisualStyleBackColor = true;
            this.Load_button.Click += new System.EventHandler(this.Load_button_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(22, 179);
            this.label3.Name = "label3";
            this.label3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label3.Size = new System.Drawing.Size(90, 13);
            this.label3.TabIndex = 17;
            this.label3.Text = "MESSAGE TYPE";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(132, 179);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(86, 13);
            this.label6.TabIndex = 18;
            this.label6.Text = "MANDATORITY";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(244, 179);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(51, 13);
            this.label7.TabIndex = 19;
            this.label7.Text = "RIORITY";
            // 
            // R_numericUpDown
            // 
            this.R_numericUpDown.Enabled = false;
            this.R_numericUpDown.Location = new System.Drawing.Point(247, 201);
            this.R_numericUpDown.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.R_numericUpDown.Name = "R_numericUpDown";
            this.R_numericUpDown.Size = new System.Drawing.Size(52, 20);
            this.R_numericUpDown.TabIndex = 20;
            // 
            // MT_comboBox
            // 
            this.MT_comboBox.AccessibleDescription = "selectbox";
            this.MT_comboBox.AccessibleName = "";
            this.MT_comboBox.AllowDrop = true;
            this.MT_comboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.MT_comboBox.Enabled = false;
            this.MT_comboBox.FormattingEnabled = true;
            this.MT_comboBox.Items.AddRange(new object[] {
            "T",
            "P"});
            this.MT_comboBox.Location = new System.Drawing.Point(25, 200);
            this.MT_comboBox.Name = "MT_comboBox";
            this.MT_comboBox.Size = new System.Drawing.Size(87, 21);
            this.MT_comboBox.TabIndex = 21;
            // 
            // M_comboBox
            // 
            this.M_comboBox.AccessibleDescription = "selectbox";
            this.M_comboBox.AccessibleName = "";
            this.M_comboBox.AllowDrop = true;
            this.M_comboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.M_comboBox.Enabled = false;
            this.M_comboBox.FormattingEnabled = true;
            this.M_comboBox.Items.AddRange(new object[] {
            "Y",
            "N"});
            this.M_comboBox.Location = new System.Drawing.Point(135, 199);
            this.M_comboBox.Name = "M_comboBox";
            this.M_comboBox.Size = new System.Drawing.Size(87, 21);
            this.M_comboBox.TabIndex = 22;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel1.AutoScroll = true;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(23, 97);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(812, 75);
            this.flowLayoutPanel1.TabIndex = 17;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(847, 630);
            this.Controls.Add(this.M_comboBox);
            this.Controls.Add(this.MT_comboBox);
            this.Controls.Add(this.R_numericUpDown);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.Load_button);
            this.Controls.Add(this.add_bi_button);
            this.Controls.Add(this.bi_comboBox);
            this.Controls.Add(this.button15);
            this.Controls.Add(this.add_dw_button);
            this.Controls.Add(this.dw_comboBox);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.Browsebutton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.export_button);
            this.Controls.Add(this.clear_button);
            this.Controls.Add(this.save_button);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Prime Credit Statement Setup System";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.R_numericUpDown)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button Browsebutton;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button save_button;
        private System.Windows.Forms.Button export_button;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox dw_comboBox;
        private System.Windows.Forms.Button add_dw_button;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Button add_bi_button;
        private System.Windows.Forms.ComboBox bi_comboBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button clear_button;
        private System.Windows.Forms.Button Load_button;
        private System.Windows.Forms.NumericUpDown R_numericUpDown;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox MT_comboBox;
        private System.Windows.Forms.ComboBox M_comboBox;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
    }
}

