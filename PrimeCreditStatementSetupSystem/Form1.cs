﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

//Import
using System.IO;
using Excel = Microsoft.Office.Interop.Excel;//Import Excel
using System.Runtime.InteropServices;
using System.Reflection;


namespace PrimeCreditStatementSetupSystem
{
    public partial class Form1 : Form
    {
        String locationPath = System.AppDomain.CurrentDomain.BaseDirectory;
        string inputPath = string.Empty; // Input_File Path
        string outputPath = string.Empty; // Output_File Path
        string fileExtension = ".xlsx"; // File Extension
        string STMT = string.Empty; //Statement
        int ExcelContentBoxtotal = 0; //ExcelContentBox Total
        int[] boxorder = new int[] { 0, 0 };// Box Order  
        List<List<string>> checkboxLists_BI = new List<List<string>>();
        List<List<string>> checkboxLists_DW = new List<List<string>>();
        ExcelContentBox[] ExcelContentBox = new ExcelContentBox[9999];
        int num = Properties.Settings.Default.StmtNum;
        List<List<String>> txtdate = new List<List<String>>();
        void Start()
        {
        }
        public Form1()
        {
            for (int i = 0; i <= num; i++)
            {
                txtdate.Add(new List<String> { });
            }
            InitializeComponent();
        }
        private void allbuttonset(Boolean status)
        {
            bi_comboBox.Enabled = status;
            dw_comboBox.Enabled = status;
            add_dw_button.Enabled = status;
            add_bi_button.Enabled = status;
            //STMT
            foreach (var button in this.flowLayoutPanel1.Controls.OfType<Button>())
            {
                button.Enabled = status;
            }
            R_numericUpDown.Enabled = status;
            M_comboBox.Enabled = status;
            MT_comboBox.Enabled = status;
            Load_button.Enabled = status;
            clear_button.Enabled = status;
            save_button.Enabled = status;
            export_button.Enabled = status;
        }

        private void Browse_Click(object sender, EventArgs e)
        {
            //Button[] bt_List = {  STMT01, STMT02, STMT03, STMT04, STMT05, STMT06, STMT07, STMT08, STMT09, STMT10 };
            Button[] bt_List = new Button[num];
            for (int i = 0; i < bt_List.Length; i++)
            {
                bt_List[i] = new Button();
                string stmtNumber = (i + 1).ToString();
                string stmt = "STMT" + stmtNumber.PadLeft(2, '0');
                //bt_List[i] = (Button)this.Controls[stmt];
                bt_List[i] = (Button)flowLayoutPanel1.Controls[stmt];
            }

            //restart
            STMT = string.Empty;
            ExcelContentBoxtotal = 0;
            checkboxLists_BI = new List<List<string>>();
            checkboxLists_DW = new List<List<string>>();
            M_comboBox.SelectedIndex = -1;
            M_comboBox.SelectedItem = "";
            MT_comboBox.SelectedIndex = -1;
            MT_comboBox.SelectedItem = "";
            allbuttonset(false);
            foreach (Button STMTButton in bt_List)
            {
                STMTButton.ForeColor = System.Drawing.Color.Black;
            }
            //restart end

            int size = -1;
            DialogResult result = openFileDialog1.ShowDialog(); // Show the dialog.
            if (result == DialogResult.OK) // Test result.
            {
                string file = openFileDialog1.FileName;
                try
                {
                    string text = File.ReadAllText(file);
                    size = text.Length;
                }
                catch (IOException)
                {
                }
            }
            inputPath = System.IO.Path.GetFullPath(openFileDialog1.FileName);
            textBox1.Text = inputPath;
            /*
            Console.WriteLine(size); // <-- Shows file size in debugging mode.
            Console.WriteLine(result); // <-- For debugging use.
            */
            ExcelContentBoxtotal = 0;
            boxorder[0] = 0;
            boxorder[1] = 0;
            panel1.Controls.Clear();//Clear panel
            if (GetExcelitem())
            {
                if (Makelist_DW() && Makelist_BI())
                {
                    allbuttonset(true);
                }
            }
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            int locationX = 20;
            int locationY = 100;
            int check_num = 1;
            int x = 0;

           
                for (int i = 0; i < num; i++)
                {
                    check_num++;
                    Button btn = new Button();
                    btn.Parent = this;
                    if (x == 0)
                    {
                        x = locationX;
                    }
                    else {
                        x += 80;
                    }
                    btn.Location = new System.Drawing.Point(x, locationY);
                    btn.Size = new System.Drawing.Size(75, 23);
                    btn.Name = "STMT" + (i + 1).ToString().PadLeft(2, '0');
                    btn.Text = "Stmt" + (i + 1).ToString().PadLeft(2, '0');
                    btn.Enabled = false;
                    btn.Click += new System.EventHandler(this.STMT_button_Click);
                    if (check_num > 10) {
                        check_num = 0;
                        locationY += 25;
                        x = 0;
                    }
                flowLayoutPanel1.Controls.Add(btn);
            }
        }

        private bool Show_Save_data(int num)
        {
            int saveNum = num;
            //SET
            foreach (string element in txtdate[saveNum])
            {
                string s = element;
                string[] sArray = s.Split(':');
                if (sArray[1].ToString() == "SET")
                {
                    //MESSAGE TYPE
                    if (sArray[2] =="T" || sArray[2] =="P")
                    {
                        MT_comboBox.SelectedItem = sArray[2];
                    }
                    else
                    {
                        MT_comboBox.SelectedIndex = -1;
                        MT_comboBox.SelectedItem = "";
                        MessageBox.Show("MESSAGE TYPE not found :" + sArray[2], "Error Message");
                    }
                    //MANDATORITY
                    if (sArray[3] == "Y" || sArray[3] == "N")
                    {
                        M_comboBox.SelectedItem = sArray[3];
                    }
                    else
                    {
                        M_comboBox.SelectedIndex = -1;
                        M_comboBox.SelectedItem = "";
                        MessageBox.Show("M_comboBox not found :" + sArray[3], "Error Message");
                    }
                    //RIORITY
                    int number = 0;
                    bool isNumber = int.TryParse(sArray[4], out number);
                    if (isNumber)
                    {
                        R_numericUpDown.Value = number;
                    }
                    else
                    {
                        R_numericUpDown.Value = 0;
                        MessageBox.Show("R_numericUpDown is not Number  :" + sArray[4], "Error Message");
                    }
                }
            }
            //DWSavedata
            foreach (List<string> name in checkboxLists_DW)
            {
                List<string> checkboxLists = new List<string>();
                List<string> clickcheckboxLists = new List<string>();
                bool hasitems = false;
                if (name.Count > 0)
                {
                    foreach (string element in txtdate[saveNum])
                    {
                        string s = element;
                        string[] sArray = s.Split(':');
                        if (sArray[1].ToString() == "DW")
                        {
                            if (name[0] == sArray[2].ToString())
                            {
                                clickcheckboxLists.Add(sArray[3].ToString());
                                hasitems = true;
                            }
                        }
                    }
                    for (int i = 1; i <= name.Count - 1; i++)
                    {
                        checkboxLists.Add(name[i].ToString());
                    }
                    if (hasitems == true)
                    {
                        ExcelContentBoxtotal++;
                        string[] items = checkboxLists.ToArray();
                        string[] clickitems = clickcheckboxLists.ToArray();
                        ExcelContentBox ExcelContentBox = new ExcelContentBox();
                        ExcelContentBox.LoadClb(name[0].ToString(), items, "DW", clickitems);
                        panel1.Controls.Add(ExcelContentBox);
                        ExcelContentBox.Location = new Point(((0) * 400) + 10, (boxorder[0] * 100) + 10);
                        checkboxLists = new List<string>();
                        checkboxLists.RemoveAll(it => true);//Clear
                        this.ExcelContentBox[ExcelContentBoxtotal] = ExcelContentBox;
                        boxorder[0]++;
                    }
                }
            }
            //BISavedata
            foreach (List<string> name in checkboxLists_BI)
            {
                List<string> checkboxLists = new List<string>();
                List<string> clickcheckboxLists = new List<string>();
                bool hasitems = false;
                if (name.Count > 0)
                {
                    foreach (string element in txtdate[saveNum])
                    {
                        string s = element;
                        string[] sArray = s.Split(':');
                        if (sArray[1].ToString() == "BI")
                        {
                            if (name[0] == sArray[2].ToString())
                            {
                                clickcheckboxLists.Add(sArray[3].ToString());
                                hasitems = true;
                            }
                        }
                    }
                    for (int i = 1; i <= name.Count - 1; i++)
                    {
                        checkboxLists.Add(name[i].ToString());
                    }
                    if (hasitems == true)
                    {
                        ExcelContentBoxtotal++;
                        string[] items = checkboxLists.ToArray();
                        string[] clickitems = clickcheckboxLists.ToArray();
                        ExcelContentBox ExcelContentBox = new ExcelContentBox();
                        ExcelContentBox.LoadClb(name[0].ToString(), items, "BI", clickitems);
                        panel1.Controls.Add(ExcelContentBox);
                        ExcelContentBox.Location = new Point(((1) * 400) + 10, (boxorder[1] * 100) + 10);
                        checkboxLists = new List<string>();
                        checkboxLists.RemoveAll(it => true);//Clear
                        this.ExcelContentBox[ExcelContentBoxtotal] = ExcelContentBox;
                        boxorder[1]++;
                    }
                }
            }
            return true;
        }
        private void STMT_button_Click(object sender, EventArgs e)
        {
            Button[] bt_List = new Button[num];
            for (int i = 0; i < bt_List.Length; i++)
            {
                bt_List[i] = new Button();
                string stmt = "STMT" + (i + 1).ToString().PadLeft(2, '0');
                //bt_List[i] = (Button)this.Controls[stmt];
                bt_List[i] = (Button)flowLayoutPanel1.Controls[stmt];
            }

            var button = sender as Button;
            int STMTNum = 0;
            STMT = button.Name;
            groupBox1.Text = (STMT + " Content");
            foreach (Button STMTButton in bt_List)
            {
                STMTButton.BackColor = System.Drawing.SystemColors.Control;
            }
            button.BackColor = System.Drawing.SystemColors.ActiveCaption;
            for (int i = 0; i < bt_List.Length; i++) {
                string checkSTMTName = "STMT" + (i + 1).ToString().PadLeft(2, '0');
                if (STMT.Equals(checkSTMTName)) {
                    STMTNum = txtdate[(i + 1)].Count > 0 ? (i + 1) : 0;
                    break;
                }
            }
            /**********************************************************/
            if (STMTNum != 0)
            {
                ExcelContentBoxtotal = 0;
                boxorder[0] = 0;
                boxorder[1] = 0;
                panel1.Controls.Clear();//Clear panel1
                //Clear Selection
                bi_comboBox.SelectedIndex = -1;
                dw_comboBox.SelectedIndex = -1;
                Show_Save_data(STMTNum);
            }
            else
            {

            }
        }
        private bool GetExcelitem()
        {
            try
            {
                /*Excel File*/
                Excel.Application xlApp = new Excel.Application();
                Excel.Workbook xlWorkbook = xlApp.Workbooks.Open(inputPath);
                Excel._Worksheet xlWorksheet = xlWorkbook.Sheets[1];
                Excel.Range xlRange = xlWorksheet.UsedRange;

                Excel._Worksheet xlWorksheet2 = xlWorkbook.Sheets[2];
                Excel.Range xlRange2 = xlWorksheet2.UsedRange;

                //Excel row & col
                int rowCount = xlRange.Rows.Count;
                int colCount = xlRange.Columns.Count;
                int rowCount2 = xlRange2.Rows.Count;
                int colCount2 = xlRange2.Columns.Count;
                List<string> checkboxLists = new List<string>();

                String type = xlWorksheet.Name;

                for (int col = 1; col <= colCount; col++)
                {
                    for (int row = 1; row <= rowCount; row++)
                    {
                        if (xlRange.Cells[col, row] != null && xlRange.Cells[row, col].Value2 != null)
                        {
                            checkboxLists.Add(xlRange.Cells[row, col].Value2.ToString());
                        }
                    }
                    checkboxLists_DW.Add(checkboxLists);
                    checkboxLists = new List<string>();
                }

                for (int col = 1; col <= colCount2; col++)
                {
                    for (int row = 1; row <= rowCount2; row++)
                    {
                        if (xlRange2.Cells[col, row] != null && xlRange2.Cells[row, col].Value2 != null)
                        {
                            checkboxLists.Add(xlRange2.Cells[row, col].Value2.ToString());
                        }
                    }
                    checkboxLists_BI.Add(checkboxLists);
                    checkboxLists = new List<string>();
                }
                return true;
            }
            catch
            {
                if (inputPath != null && inputPath.IndexOf(fileExtension) == -1)
                {
                    MessageBox.Show("This file type is not allowed. The allowed extensions are:" + string.Join("、", fileExtension.ToString()), "Error Message");
                    return false;
                }
                else
                {
                    MessageBox.Show("File not found,Please upload file", "Error Message");
                    return false;
                }
            }
        }
        private bool Makelist_DW()
        {
            dw_comboBox.DataSource = null;
            int i = 0;
            DataTable list = new DataTable();
            list.Columns.Add(new DataColumn("Display", typeof(string)));
            list.Columns.Add(new DataColumn("ID", typeof(int)));
            foreach (List<string> element in checkboxLists_DW)
            {
                if (element.Count > 0)
                {
                    Console.WriteLine(i);
                    Console.WriteLine(element);
                    Console.WriteLine(element[0]);
                    list.Rows.Add(list.NewRow());
                    list.Rows[i][0] = element[0].ToString();
                    list.Rows[i][1] = i + 1;
                }
                i++;
            }
            dw_comboBox.DataSource = list;
            dw_comboBox.DisplayMember = "Display";
            dw_comboBox.ValueMember = "ID";
            dw_comboBox.SelectedIndex = -1;
            return true;
        }
        private bool Makelist_BI()
        {
            bi_comboBox.DataSource = null;
            int i = 0;
            DataTable list = new DataTable();
            list.Columns.Add(new DataColumn("Display", typeof(string)));
            list.Columns.Add(new DataColumn("ID", typeof(int)));
            foreach (List<string> element in checkboxLists_BI)
            {
                if (element.Count > 0)
                {
                    list.Rows.Add(list.NewRow());
                    list.Rows[i][0] = element[0].ToString();
                    list.Rows[i][1] = i + 1;
                }
                i++;
            }
            bi_comboBox.DataSource = list;
            bi_comboBox.DisplayMember = "Display";
            bi_comboBox.ValueMember = "ID";
            bi_comboBox.SelectedIndex = -1;
            return true;
        }

        private void Savebutton_Click(object sender, EventArgs e)
        {
            if (STMT==null || STMT == "")
            {
                MessageBox.Show("Please select Statement", "Message");
            }
            else if (MT_comboBox.SelectedItem == null)
            {
                MessageBox.Show("Please select MESSAGE TYPE", "Message");
            }
            else if (M_comboBox.SelectedItem == null)
            {
                MessageBox.Show("Please select MANDATORITY", "Message");
            }
            else
            {
                try
                {
                    List<string> txtdate_DW = new List<string>();
                    List<string> txtdate_BI = new List<string>();
                    outputPath = @locationPath + @"\Output_File\" + STMT + ".txt";
                    //outputPath = @"C:\Ken\FXHK\PrimeCreditStatementSetupSystem\Output_File\Output" + DateTime.Now.ToString("yyyy-MM-dd_HHmm") + ".txt";
                    for (int i = 1; i <= ExcelContentBoxtotal; i++)
                    {
                        foreach (string element in this.ExcelContentBox[i].getcheckedboxinfo())
                        {
                            switch (this.ExcelContentBox[i].getTextboxType())
                            {
                                case "DW":
                                    txtdate_DW.Add(STMT + ":" + this.ExcelContentBox[i].getTextboxType() + ":" + this.ExcelContentBox[i].getTextboxName() + ":" + element);
                                    break;
                                case "BI":
                                    txtdate_BI.Add(STMT + ":" + this.ExcelContentBox[i].getTextboxType() + ":" + this.ExcelContentBox[i].getTextboxName() + ":" + element);
                                    break;
                                default:
                                    MessageBox.Show("Not Found Type :" + this.ExcelContentBox[i].getTextboxType(), "Error Message");
                                    break;
                            }
                        }

                        for (int selectedStmt = 0; selectedStmt < num; selectedStmt++)
                        {
                            string checkSTMTName = "STMT" + (selectedStmt + 1).ToString().PadLeft(2, '0');
                            Button btn = new Button();
                            if (STMT.Equals(checkSTMTName))
                            {
                                txtdate[(selectedStmt + 1)] = new List<string>();
                                txtdate[(selectedStmt + 1)].Add(STMT + ":SET:" + MT_comboBox.SelectedItem.ToString() + ":" + M_comboBox.SelectedItem.ToString() + ":" + R_numericUpDown.Value.ToString());
                                txtdate[(selectedStmt + 1)].AddRange(txtdate_DW.Union(txtdate_BI).ToList<string>());
                                btn = (Button)flowLayoutPanel1.Controls[checkSTMTName];
                                btn.ForeColor = System.Drawing.Color.Blue;
                                break;
                            }
                        }
                        /*************************************************************/
                    }
                    //Message
                    MessageBox.Show("Save Successful", "Message");
                }
                catch
                {
                    //MessageBox.Show("Save fail", "Error Message");
                }
            }
        }
        private void export_button_Click(object sender, EventArgs e)
        {
            String locationPath = System.AppDomain.CurrentDomain.BaseDirectory;
            //OutputTxt
            outputPath = outputPath = @locationPath + @"Output_File\Output.txt";
            try
            {
                bool haveData = false;
                txtdate[0].Clear();
                for (int i = 0; i < txtdate.Count; i++)
                {
                    if (txtdate[i].Count > 0)
                    {
                        txtdate[0].AddRange(txtdate[i]);
                        haveData = true;
                    }
                }
                if (haveData)
                {
                    OutputTxt(outputPath, txtdate[0]);
                    //Message
                    MessageBox.Show("Successful Export Text Path:" + outputPath, "Message");
                    System.Diagnostics.Process prc = new System.Diagnostics.Process();
                    prc.StartInfo.FileName = @locationPath + @"Output_File";
                    prc.Start();
                }
                else
                {
                    //Message
                    MessageBox.Show("The saved data could not be found. Please save the data before exporting the file.", "Message");
                }
            }
            catch
            {
                MessageBox.Show("Export fail", "Error Message");
            }
        }
        private void clear_button_Click(object sender, EventArgs e)
        {
            ExcelContentBoxtotal = 0;
            boxorder[0] = 0;
            boxorder[1] = 0;
            panel1.Controls.Clear();//Clear panel1
            //Clear Selection
            bi_comboBox.SelectedIndex = -1;
            dw_comboBox.SelectedIndex = -1;
        }

        public void OutputTxt(string inputPath, List<string> txtdate)
        {
            try
            {
                FileStream fs = new FileStream(inputPath, FileMode.Create);
                StreamWriter sw = new StreamWriter(fs);
                //開始寫入
                foreach (string element in txtdate)
                {
                    sw.Write(element.ToString() + "\r\n");
                }
                //清空緩衝區
                sw.Flush();
                //關閉流
                sw.Close();
                fs.Close();
            }
            catch
            {
                MessageBox.Show("Error Saving Text File", "Error Message");
            }
        }

        private void add_dw_button_Click(object sender, EventArgs e)
        {
            if (dw_comboBox.SelectedItem != null)
            {
                DataRowView oDataRowView = dw_comboBox.SelectedItem as DataRowView;
                string listName = string.Empty;
                if (oDataRowView != null)
                {
                    listName = oDataRowView.Row["Display"] as string;
                }
                int selectedValue = (int.Parse(dw_comboBox.SelectedValue.ToString()) - 1);
                int listlength = checkboxLists_DW[selectedValue].Count;
                Addbox(1, listName, selectedValue, listlength, "DW");
                //Clear Selection
                dw_comboBox.SelectedIndex = -1;
            }
            else
            {
                MessageBox.Show("Please select an item from the list", "Message");
            }
        }

        private void add_bi_button_Click(object sender, EventArgs e)
        {
            if (bi_comboBox.SelectedItem != null)
            {
                DataRowView oDataRowView = bi_comboBox.SelectedItem as DataRowView;
                string listName = string.Empty;
                if (oDataRowView != null)
                {
                    listName = oDataRowView.Row["Display"] as string;
                }
                int selectedValue = (int.Parse(bi_comboBox.SelectedValue.ToString()) - 1);
                int listlength = checkboxLists_BI[selectedValue].Count;
                Addbox(2, listName, selectedValue, listlength, "BI");
                //Clear Selection
                bi_comboBox.SelectedIndex = -1;
            }
            else
            {
                MessageBox.Show("Please select an item from the list", "Message");
            }
        }

        private bool Addbox(int Page, String TextName, int selectedValue, int listlength, String type)
        {
            List<string> checkboxLists = new List<string>();
            switch (type)
            {
                case "DW":
                    for (int i = 1; i <= listlength - 1; i++)
                    {
                        checkboxLists.Add(checkboxLists_DW[selectedValue][i].ToString());
                    }
                    break;
                case "BI":
                    for (int i = 1; i <= listlength - 1; i++)
                    {
                        checkboxLists.Add(checkboxLists_BI[selectedValue][i].ToString());
                    }
                    break;
                default:
                    MessageBox.Show("Not Found Type :" + type, "Error Message");
                    return false;
            }
            ExcelContentBoxtotal++;
            string[] items = checkboxLists.ToArray();
            ExcelContentBox ExcelContentBox = new ExcelContentBox();
            ExcelContentBox.setClb(TextName, items, type);
            panel1.Controls.Add(ExcelContentBox);
            ExcelContentBox.Location = new Point(((Page - 1) * 400) + 10, (boxorder[Page - 1] * 100) + 10);
            checkboxLists = new List<string>();
            checkboxLists.RemoveAll(it => true);//Clear
                                                //Console.WriteLine(ExcelContentBoxtotal);
            this.ExcelContentBox[ExcelContentBoxtotal] = ExcelContentBox;
            boxorder[Page - 1]++;
            return true;
        }
        //Load txt
        private void Load_button_Click(object sender, EventArgs e)
        {
            txtdate = new List<List<String>>();
            for (int i = 0; i <= num; i++)
            {
                txtdate.Add(new List<String> { });
            }
            Loadtxt();
        }
        private void Loadtxt()
        {
            try
            {
                // Read the TXT file
                String LoadtxtPath = @locationPath + @"\Output_File\Output.txt";
                StreamReader str = new StreamReader(LoadtxtPath);
                string Messagebox = string.Empty;
                string line;
                string[] lineArray;
                while ((line = str.ReadLine()) != null)
                {
                    lineArray = line.Split(':');
                    for (int selectedStmt = 0; selectedStmt <= num; selectedStmt++)
                    {
                        string checkSTMTName = "STMT" + (selectedStmt + 1).ToString().PadLeft(2, '0');
                        Button btn = new Button();
                        if (lineArray[0] == checkSTMTName)
                        {
                            if (LoadChecked(selectedStmt, lineArray, line) == false)
                            {
                                Messagebox = Messagebox + line.ToString() + Environment.NewLine;
                            }
                            break;
                        }
                    }
                }
                if (Messagebox != "")
                {
                    MessageBox.Show("Not Found STMT :" + Environment.NewLine + Messagebox, "Error Message");
                }
                str.Close();
            }
            catch
            {
                MessageBox.Show("Error Load Text File", "Error Message");
            }
        }
        private bool LoadChecked(int saveNum,string[] lineArray, string line)
        {
            //Button[] bt_List = {  STMT02, STMT03, STMT04, STMT05, STMT06, STMT07, STMT08, STMT09, STMT10 };
            Button[] bt_List = new Button[num];
            for (int i = 0; i < bt_List.Length; i++)
            {
                bt_List[i] = new Button();
                string stmtNumber = (i + 1).ToString();
                string stmt = "STMT" + stmtNumber.PadLeft(2, '0');
                bt_List[i] = (Button)flowLayoutPanel1.Controls[stmt];
            }

            bool nameChecked = false;
            bool itmeChecked = false;
            if (lineArray[1] == "SET")
            {
                txtdate[saveNum + 1].Add(line.ToString());
                itmeChecked = true;
                nameChecked = true;
            }
            else if (lineArray[1] == "DW")
            {
                foreach (List<string> name in checkboxLists_DW)
                {
                    if (name.Count > 0)
                    {
                        //Check name
                        if (name[0] == lineArray[2])
                        {
                            nameChecked = true;
                            for (int i = 0; i < name.Count; i++)
                            {
                                //Check item
                                if (name[i] == lineArray[3])
                                {
                                    txtdate[saveNum + 1].Add(line.ToString());
                                    bt_List[saveNum].ForeColor = System.Drawing.Color.Blue;
                                    itmeChecked = true;
                                }
                            }
                        }
                    }
                }
            }
            else if (lineArray[1] == "BI")
            {
                foreach (List<string> name in checkboxLists_BI)
                {
                    if (name.Count > 0)
                    {
                        //Check name
                        if (name[0] == lineArray[2])
                        {
                            nameChecked = true;
                            for (int i = 0; i < name.Count; i++)
                            {
                                //Check item
                                if (name[i] == lineArray[3])
                                {
                                    txtdate[saveNum + 1].Add(line.ToString());
                                    bt_List[saveNum].ForeColor = System.Drawing.Color.Blue;
                                    itmeChecked = true;
                                }
                            }
                        }
                    }
                }
            }

            if (nameChecked == false || itmeChecked == false)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}
