﻿namespace PrimeCreditStatementSetupSystem
{
    partial class ExcelContentBox
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BoxName = new System.Windows.Forms.TextBox();
            this.checkedListBox1 = new System.Windows.Forms.CheckedListBox();
            this.type_label = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // BoxName
            // 
            this.BoxName.Location = new System.Drawing.Point(3, 10);
            this.BoxName.Name = "BoxName";
            this.BoxName.ReadOnly = true;
            this.BoxName.Size = new System.Drawing.Size(195, 20);
            this.BoxName.TabIndex = 0;
            // 
            // checkedListBox1
            // 
            this.checkedListBox1.FormattingEnabled = true;
            this.checkedListBox1.Location = new System.Drawing.Point(209, 10);
            this.checkedListBox1.Name = "checkedListBox1";
            this.checkedListBox1.Size = new System.Drawing.Size(180, 79);
            this.checkedListBox1.TabIndex = 1;
            // 
            // type_label
            // 
            this.type_label.AutoSize = true;
            this.type_label.Location = new System.Drawing.Point(3, 33);
            this.type_label.Name = "type_label";
            this.type_label.Size = new System.Drawing.Size(27, 13);
            this.type_label.TabIndex = 3;
            this.type_label.Text = "type";
            // 
            // ExcelContentBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.type_label);
            this.Controls.Add(this.checkedListBox1);
            this.Controls.Add(this.BoxName);
            this.Name = "ExcelContentBox";
            this.Size = new System.Drawing.Size(400, 100);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox BoxName;
        private System.Windows.Forms.CheckedListBox checkedListBox1;
        private System.Windows.Forms.Label type_label;
    }
}
