﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
//Import
using System.IO;
using System.Runtime.InteropServices;
using System.Reflection;
using System.Text.RegularExpressions;

namespace PrimeCreditDataMerge
{
    public partial class Form1 : Form
    {
        int num = Properties.Settings.Default.StmtNum;
        string locationPath = System.AppDomain.CurrentDomain.BaseDirectory;
        string inputPath = string.Empty; // Input_File Path
        string outputPath = string.Empty; // Output_File Path
        //ATDAR set
        List<string> ATDARdata = new List<string>();
        //DW set
        List<String> dwnamedata = new List<String> { };
        List<String> dwdata = new List<String> { };
        //BI set
        List<String> binamedata = new List<String> { };
        List<String> bidata = new List<String> { };
        //ATACZ set
        string ATACZstart = string.Empty;
        string ATACZend = string.Empty;
        List<String> ATACZdata = new List<String> { };
        List<String> ATACZdataorder = new List<String> { };
        //ATTXZ set
        string ATTXZstart = string.Empty;
        string ATTXZend = string.Empty;
        List<String> ATTXZdata = new List<String> { };
        List<String> groupdata = new List<String> { };
        //Datasetup set
        List<List<String>> txtdate = new List<List<String>>();
        //Output Date
        List<String> outputdata = new List<String> {};
        //Console.WriteLine();
        public Form1()
        {
            InitializeComponent();
            Shown += new EventHandler(Form1_Shown);
        }
        private void Form1_Shown(object sender, EventArgs e)
        {
            WriteLog("Programmer Startup");
            DataMerga();
        }

        private void Loadtxt(string ATTXZPath , string ATACZPath)
        {
            try
            {
                // Read the file
                StreamReader ATTXZ = new StreamReader(ATTXZPath);
                StreamReader ATACZ = new StreamReader(ATACZPath);
                string Messagebox = string.Empty;
                string line = string.Empty;
                int i = 0;
                line = ATACZ.ReadLine();
                ATACZstart = line.Trim();
                line = ATTXZ.ReadLine();
                ATTXZstart = line.Trim();
                if (ATACZstart == ATTXZstart)
                {
                    while ((line = ATACZ.ReadLine()) != null)
                    {
                        if (line.Substring(0, 2) == "99")
                        {
                            ATTXZend = line;
                        }
                        else { 
                            ATACZdata.Add(line);
                        }
                        i++;
                    }
                    OrderATACZdata();
                    while ((line = ATTXZ.ReadLine()) != null)
                    {
                        if (line.Substring(0, 2) == "99")
                        {
                            ATTXZend = line;
                        }
                        else {
                            ATTXZdata.Add(line);
                        }
                        i++;
                    }
                }
                else
                {
                    WriteLog("ATACZ ( " + ATACZstart + " ) and ATTXZ ( " + ATTXZstart + " ) Number Not Match");
                }
            }
            catch (IOException)
            {
            }
        }
        private void OrderATACZdata()
        {
            //排序
            IEnumerable<string> query = from word in ATACZdata
                                        orderby word.Replace("|", "").Substring(280, 25)
                                        select word;
            //note:遞減排序 descending
            foreach (string str in query)
            {
                ATACZdataorder.Add(str);
            }
        }
        private void Dataintegration(){
            List<string> ATACZpart = new List<String> { };
            List<string> DWdatacheck = new List<String> { };
            List<string> BIdatacheck = new List<String> { };
            //clean
            outputdata = new List<String> { };
            for (int i = 0; i < ATACZdataorder.Count; i++)
            {
                if (i != 0) {
                    if (ATACZdataorder[i] != "" && ATACZdataorder[i].Substring(280, 25).Trim() != "" && ATACZdataorder[i - 1].Substring(280, 25) == ATACZdataorder[i].Substring(280, 25))
                    {
                        ATACZpart.Add(ATACZdataorder[i]);
                    }
                    else
                    {
                        foreach (string ATACZpart_data in ATACZpart)
                        {
                            var ATTXZdataList = ATTXZdata.Where(x => x.Substring(3, 19) == ATACZpart_data.Substring(3, 19)).ToList();
                            if (ATTXZdataList != null && ATTXZdataList.Count > 0)
                            {
                                groupdata.Add(ATACZFormat(ATACZpart_data));
                                groupdata.AddRange(ATTXZFormat(ATTXZdataList));
                            }
                        }
                        if (groupdata != null && groupdata.Count > 0) {
                            string customerid = ATACZdataorder[i - 1].Substring(280, 25).Trim();
                            outputdata.Add("ATSTR|");
                            //ATDAR
                            outputdata.AddRange( SetATDARdata(customerid,ATDARdata) );
                            //DW
                            var DWdataList = dwdata.Where(x => x.Split('|')[2].Trim() == customerid);
                            if (DWdataList != null && DWdataList.ToList().Count >0)
                            {
                                outputdata.AddRange(DWdataList.ToList());
                                DWdatacheck.AddRange(DWdataList.ToList());
                            }
                            //BI
                            var BIdataList = bidata.Where(x =>  x.Split('|')[1].Trim() == customerid);
                            if (BIdataList != null && BIdataList.ToList().Count > 0)
                            {
                                outputdata.AddRange(BIdataList.ToList());
                                BIdatacheck.AddRange(BIdataList.ToList());
                            }
                            //ATMSG
                            outputdata.AddRange(SetupCheckeddata(customerid, DWdatacheck, BIdatacheck));
                            //ATACZ & ATTXZ
                            outputdata.AddRange(groupdata);
                            outputdata.Add("ATEND|");
                            //clean
                            ATACZpart = new List<String> { };
                            groupdata = new List<String> { };
                            DWdatacheck = new List<String> { };
                            BIdatacheck= new List<String> { };
                        }
                        ATACZpart.Add(ATACZdataorder[i]);
                    }
                }
                else
                {
                    ATACZpart.Add(ATACZdataorder[i]);
                }
            }
            //OutputdataFormat();
        }
        //ATDAR-----------------------------------------------------------------------------------------------------------
        private void LoadATDARdata(string ATDARfilePath)
        {
            FileStream ATDARFiles = new FileStream(ATDARfilePath, System.IO.FileMode.Open, System.IO.FileAccess.Read);
            StreamReader ATDARReader = new StreamReader(ATDARFiles, Encoding.Default);
            //記錄每次讀取的一行記錄
            ATDARdata = new List<String> { };
            string strLine = "";
            //記錄每行記錄中的各字段內容
            string[] aryLine = null;
            //標示列數
            int columnCount = 0;
            //逐行讀取CSV中的數據
            while ((strLine = ATDARReader.ReadLine()) != null)
            {
                aryLine = strLine.Split(',');
                columnCount = aryLine.Length;
                string saveline = string.Empty;
                for (int j = 0; j < columnCount; j++)
                {
                    aryLine[j] = aryLine[j].Replace("\"", "");
                    saveline = saveline + "|" + aryLine[j].Replace("\"", "").Trim();
                }
                saveline = "ATDAR" + saveline.Replace("\'", "");
                ATDARdata.Add(saveline);
            }
            ATDARReader.Close();
            ATDARFiles.Close();
        }
        private List<string> SetATDARdata(string id, List<string> ATDARdata)
        {
            List<string> Setdatalist = new List<string>{ };
            foreach (string data in ATDARdata)
            {
                string[] dataArray = data.Split('|');
                if (dataArray[1] == id)
                {
                    Setdatalist.Add(data);
                }
            }
            return Setdatalist;
        }
        //----------------------------------------------------------------------------------------------------------------
        private List<string> SetupCheckeddata(string id , List<string> DWdata , List<string> BIdata)
        {
            List<string> Checkeddata = new List<string> { };
            int Checkedtime = 0;
            string setdate = "";
            //STMT
            for (int j = 1; j < txtdate.Count; j++)
            {
                Checkedtime = 0;
                //Setupdata Output.txt
                foreach (string data2 in txtdate[j])
                {
                    string setupdata = data2;
                    string[] setupdataArray = setupdata.Split(':');
                    if (setupdataArray[1].ToString() == "SET")
                    {
                        setdate = setupdataArray[2].ToString() + "|" + setupdataArray[3].ToString() + "|" + setupdataArray[4].ToString();
                        Checkedtime++;
                    }
                    else if (setupdataArray[1].ToString() == "DW")
                    {
                        // DW|XXXX data
                        foreach (string data3 in DWdata)
                        {
                            string dwitemdata = data3;
                            string[] dwitemdataArray = dwitemdata.Split('|');
                            for (int i = 0; i < dwnamedata.Count; i++)
                            {
                                if(setupdataArray[2] == dwnamedata[i])
                                {
                                    if (setupdataArray[3] == dwitemdataArray[i])
                                    {
                                        Checkedtime++;
                                    }
                                    else if ((setupdataArray[3].Contains("=") || setupdataArray[3].Contains(">") || setupdataArray[3].Contains("<")))
                                    {
                                        string numdata = data3;
                                        //string[] numdataArray = dwitemdataArray[i].Split("||");
                                        List<string> numdataArray = new List<string>(setupdataArray[3].Split(new string[] { "||" },StringSplitOptions.None ));
                                        if(numdataArray.Count >= 7)
                                        {
                                            if ( ( numdataArray[1].Contains(">=") && numdataArray[5].Contains("<=")) )
                                            {
                                                //FLD||>=||100||&&||FLD||<=||1000
                                                if(dwitemdataArray[i] != "" && dwitemdataArray[i] != null) {
                                                    bool Logical = ( (int.Parse(dwitemdataArray[i]) >= int.Parse(numdataArray[2])) && ( int.Parse(dwitemdataArray[i]) <= int.Parse(numdataArray[6] )) );
                                                    if (Logical)
                                                    {
                                                        Checkedtime++;
                                                    }
                                                }
                                            }
                                            else if((numdataArray[1].Contains(">") && numdataArray[5].Contains("<")))
                                            {
                                                //FLD||>=||100||&&||FLD||<=||1000
                                                if (dwitemdataArray[i] != "" && dwitemdataArray[i] != null)
                                                {
                                                    bool Logical = ((int.Parse(dwitemdataArray[i]) > int.Parse(numdataArray[2])) && (int.Parse(dwitemdataArray[i]) < int.Parse(numdataArray[6])));
                                                    if (Logical)
                                                    {
                                                        Checkedtime++;
                                                    }
                                                }
                                            }
                                        }
                                        else if (numdataArray.Count <= 3)
                                        {
                                            if ( numdataArray[1].Contains("<=") )
                                            {
                                                //FLD||>=||100||&&||FLD||<=||1000
                                                bool Logical = ((int.Parse(dwitemdataArray[i]) <= int.Parse(numdataArray[3])) );
                                                if (Logical)
                                                {
                                                    Checkedtime++;
                                                }
                                            }
                                            else if( numdataArray[1].Contains(">=")  )
                                            {
                                                //FLD||>=||100||&&||FLD||<=||1000
                                                bool Logical = ((int.Parse(dwitemdataArray[i]) >= int.Parse(numdataArray[3])) );
                                                if (Logical)
                                                {
                                                    Checkedtime++;
                                                }
                                            }
                                            else if( numdataArray[1].Contains(">") )
                                            {
                                                //FLD||>=||100||&&||FLD||<=||1000
                                                bool Logical = ( (int.Parse(dwitemdataArray[i]) > int.Parse(numdataArray[3])) );
                                                if (Logical)
                                                {
                                                    Checkedtime++;
                                                }
                                            }
                                            else if( numdataArray[1].Contains("<") )
                                            {
                                                bool Logical = ((int.Parse(dwitemdataArray[i]) < int.Parse(numdataArray[3])) );
                                                if (Logical)
                                                {
                                                    Checkedtime++;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    // BI|XXXX data
                    else if (setupdataArray[1].ToString() == "BI")
                    {
                        foreach (string data3 in BIdata)
                        {
                            string biitemdata = data3;
                            string[] biitemdataArray = biitemdata.Split('|');
                            for (int i = 0; i < binamedata.Count; i++)
                            {
                                if (setupdataArray[2] == binamedata[i] && setupdataArray[3] == biitemdataArray[i])
                                {
                                    Checkedtime++;
                                }
                                else if ((setupdataArray[3].Contains("=") || setupdataArray[3].Contains(">") || setupdataArray[3].Contains("<")))
                                {
                                    string numdata = data3;
                                    List<string> numdataArray = new List<string>(setupdataArray[3].Split(new string[] { "||" }, StringSplitOptions.None));
                                    if (numdataArray.Count >= 7)
                                    {
                                        if ((numdataArray[1].Contains(">=") && numdataArray[5].Contains("<=")))
                                        {
                                            //FLD||>=||100||&&||FLD||<=||1000
                                            bool Logical = ((int.Parse(biitemdataArray[i]) >= int.Parse(numdataArray[2])) && (int.Parse(biitemdataArray[i]) <= int.Parse(numdataArray[6])));
                                            if (Logical)
                                            {
                                                Checkedtime++;
                                            }
                                        }
                                        else if ((numdataArray[1].Contains(">") && numdataArray[5].Contains("<")))
                                        {
                                            //FLD||>=||100||&&||FLD||<=||1000
                                            bool Logical = ((int.Parse(biitemdataArray[i]) > int.Parse(numdataArray[2])) && (int.Parse(biitemdataArray[i]) < int.Parse(numdataArray[6])));
                                            if (Logical)
                                            {
                                                Checkedtime++;
                                            }
                                        }
                                    }
                                    else if (numdataArray.Count <= 3)
                                    {
                                        if (numdataArray[1].Contains("<="))
                                        {
                                            //FLD||>=||100||&&||FLD||<=||1000
                                            bool Logical = ((int.Parse(biitemdataArray[i]) <= int.Parse(numdataArray[3])));
                                            if (Logical)
                                            {
                                                Checkedtime++;
                                            }
                                        }
                                        else if (numdataArray[1].Contains(">="))
                                        {
                                            //FLD||>=||100||&&||FLD||<=||1000
                                            bool Logical = ((int.Parse(biitemdataArray[i]) >= int.Parse(numdataArray[3])));
                                            if (Logical)
                                            {
                                                Checkedtime++;
                                            }
                                        }
                                        else if (numdataArray[1].Contains(">"))
                                        {
                                            //FLD||>=||100||&&||FLD||<=||1000
                                            bool Logical = ((int.Parse(biitemdataArray[i]) > int.Parse(numdataArray[3])));
                                            if (Logical)
                                            {
                                                Checkedtime++;
                                            }
                                        }
                                        else if (numdataArray[1].Contains("<"))
                                        {
                                            bool Logical = ((int.Parse(biitemdataArray[i]) < int.Parse(numdataArray[3])));
                                            if (Logical)
                                            {
                                                Checkedtime++;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    //Checked
                    if (txtdate[j].Count > 0 && Checkedtime == txtdate[j].Count)
                    {
                        if (j < 10) {
                            Checkeddata.Add("ATMSG|" + id + "|STMT0" + j + "|" + setdate);
                        }
                        else
                        {
                            Checkeddata.Add("ATMSG|" + id + "|STMT" + j + "|" + setdate);
                        }
                    }
                }
            }
            return Checkeddata;
        }
        private void GetSetupdata(string LoadtxtPath)
        {
            try
            {
                for (int i = 0; i <= num; i++)
                {
                    txtdate.Add(new List<string> { });
                }
                // Read the TXT file
                StreamReader str = new StreamReader(LoadtxtPath);
                string Messagebox = string.Empty;
                string line;
                string[] lineArray;
                while ((line = str.ReadLine()) != null)
                {
                    lineArray = line.Split(':');
                    string chacknum = Regex.Replace(lineArray[0], "[^0-9]", "");
                    int number = 0;
                    bool isNumber = int.TryParse(chacknum, out number);
                    if (lineArray[0].IndexOf("STMT") == -1 || isNumber && number > txtdate.Count)
                    {
                        Messagebox = Messagebox + line.ToString() + Environment.NewLine;
                    }
                    else
                    {
                        for (int selectedStmt = 0; selectedStmt <= num; selectedStmt++)
                        {
                            string checkSTMTName = "STMT" + (selectedStmt + 1).ToString().PadLeft(2, '0');
                            if (lineArray[0] == checkSTMTName)
                            {
                                txtdate[selectedStmt + 1].Add(line.ToString());
                                break;
                            }
                        }
                    }
                }
                if (Messagebox != "")
                {
                    WriteLog("Not Found STMT: " + Environment.NewLine + Messagebox);
                }
                str.Close();
            }
            catch
            {
                WriteLog("Error Load Text File");
            }
        }
        private string ATACZFormat(string date)
        {
            List<int> ATACZFormatData = new List<int> { 1, 3, 22, 25, 28, 68, 108, 148, 188, 228, 268, 271, 281, 306, 366, 426, 429, 431, 432, 433, 452, 471, 490, 509, 522, 524, 536, 543, 562, 581, 589, 597, 605, 608, 609, 612, 614, 623, 632, 641, 650, 651, 660, 669, 678, 687, 696, 697, 702, 721, 740, 741, 749, 768, 787, 788, 807, 826, 845, 864, 883, 903, 906, 915, 918, 923, 942, 945, 964, 967, 986, 1005, 1024, 1043, 1045, 1047, 1049, 1051, 1053, 1055, 1057, 1059, 1061, 1063, 1065, 1067, 1069, 1071, 1090, 1109, 1128, 1129, 1148, 1156, 1164, 1172, 1180, 1188, 1196, 1204, 1212, 1220, 1228, 1236, 1244, 1252, 1260, 1279, 1298, 1317, 1336, 1355, 1374, 1393, 1412, 1431, 1450, 1469, 1488, 1507, 1526, 1556, 1557, 1568, 1579, 1590, 1601, 1612, 1623, 1625, 1627, 1629, 1631, 1650, 1652, 1671, 1690, 1745, 1746, 1749, 1768, 1771, 1779, 1787, 1788, 1791, 1794, 1797, 1800, 1803, 1806, 1809, 1812, 1813, 1832, 1836, 1840, 1844, 1848, 1855, 1864, 1883, 1902, 1921, 1940, 1947, 1966, 1967, 1968, 1969, 1970, 1980, 1999, 2004, 2023, 2026, 2045, 2048, 2067, 2086, 2089, 2108, 2127, 2146, 2165, 2184, 2203, 2222, 2241, 2260, 2261, 2262, 2281, 2282, 2301, 2320, 2339, 2358, 2377, 2396, 2415, 2434, 2453, 2472, 2491, 2510, 2529, 2548, 2567, 2586, 2605, 2624, 2643, 2662, 2681, 2700, 2708, 2727, 2746, 2765, 2784, 2803, 2822, 2841, 2860, 2879, 2898, 2917, 2936, 2955, 2974, 2993, 3012, 3031, 3050, 3058, 3078, 3098, 3118, 3135, 3152, 3153, 3154, 3155, 3172, 3173, 3174, 3191, 3195, 3196, 3197, 3198, 3215, 3216, 3217, 3234, 3294, 3295, 3312, 3332, 3351, 3368, 3369, 3373, 3375, 3415, 3455, 3495, 3496, 3503, 3510, 3517, 3524, 3525, 3526, 3527, 3528, 3558, 3559, 3578, 3597, 3598, 3617, 3623, 3632, 3641, 3650 };
            string formatdata = string.Empty;
            formatdata = date;
            for (int i = 0; i < ATACZFormatData.Count; i++)
            {
                formatdata = formatdata.Insert(ATACZFormatData[i] - 1 + i, "|");
            }
            return "ATACZ" + formatdata;
        }
        private List<string> ATTXZFormat(List<string> date)
        {
            List<int> ATTXZFormatData = new List<int> { 1, 3, 22, 25, 30, 33, 41, 42, 45, 64, 83, 91, 99, 102, 108, 111, 120, 125, 137, 152, 168, 183, 188, 211, 251, 259, 260, 261, 266, 267, 270, 279, 284, 289, 298, 300, 314, 315, 319, 324, 329, 334, 336, 339, 341, 356, 365, 366, 385, 390, 391, 407, 409, 411, 431, 450, 465, 466, 467, 486, 494, 513, 532, 533, 536, 537 };
            List<string> formatdata = new List<String> { };
            formatdata = date;
            for (int j = 0; j < formatdata.Count; j++)
            {
                for (int i = 0; i < ATTXZFormatData.Count; i++)
                {
                    formatdata[j] = formatdata[j].Insert(ATTXZFormatData[i] - 1 + i, "|");
                }
                formatdata[j] = "ATTXZ" + formatdata[j];
            }
            return formatdata;
        }
        private void Outputtxt(string OutputPath, string ATTXZstart, List<String>date)
        {
            //OutputTxt
            try
            {
                FileStream fs = new FileStream(OutputPath, FileMode.Create);
                StreamWriter sw = new StreamWriter(fs);
                int txtRows = 0;
                //開始寫入
                sw.Write(ATTXZstart.ToString() + "\r\n");
                txtRows++;
                foreach (string element in date)
                {
                    sw.Write(element.ToString() + "\r\n");
                    txtRows++;
                }
                sw.Write(txtRows.ToString() + "\r\n");
                //清空緩衝區
                sw.Flush();
                //關閉流
                sw.Close();
                fs.Close();
            }
            catch (IOException)
            {
                WriteLog("Error Output Text File");
            }
        }

        private void DataMerga()
        {
            try
            {
                //Path set
                string DW_CSV_Path = Properties.Settings.Default.DW_CSV_Path;
                string BI_CSV_Path = Properties.Settings.Default.BI_CSV_Path;
                string DAR_CSV_Path = Properties.Settings.Default.DAR_CSV_Path;
                string ATTXZPath = Properties.Settings.Default.ATTXZPath;
                string ATACZPath = Properties.Settings.Default.ATACZPath;
                string LoadtxtPath = Properties.Settings.Default.LoadtxtPath;
                string outputPath = Properties.Settings.Default.outputPath;
                //Main
                //OpenCSV
                OpenCSV(DW_CSV_Path, BI_CSV_Path);
                MessageText("OpenCSV OK(1/5)");
                //LoadATDARdata
                LoadATDARdata(DAR_CSV_Path);
                MessageText("LoadATDARdata OK(2/6)");
                //Loadtxt
                Loadtxt(ATTXZPath,ATACZPath);
                MessageText("Loadtxt OK(3/6)");
                //GetSetupdata
                GetSetupdata(LoadtxtPath);
                MessageText("GetSetupdata OK(4/6)");
                //Dataintegration
                Dataintegration();
                MessageText("Dataintegration OK(5/6)");
                if (outputPath != null || ATTXZstart != null || outputdata != null) {
                    //OutputTxt
                    Outputtxt(outputPath, ATTXZstart, outputdata);
                    MessageText("Done(6/6)");
                    //MessageBox.Show("Successful Export Text Path:" + outputPath, "Message");
                    System.Diagnostics.Process prc = new System.Diagnostics.Process();
                    prc.StartInfo.FileName = @locationPath + @"Output_File";
                    prc.Start();
                    WriteLog("Programmer is Complete");
                    MessageText("Successful Export Text Path:" + outputPath);
                }
            }
            catch
            {
                WriteLog("DataMerga Error");
            }
        }
        public void MessageText(string Message)
        {
            StatusTextBox1.AppendText(Message + "\r\n");
            Application.DoEvents();//實時響應文本框中的值 Updata TextBox
        }
        public void OpenCSV(string dwfilePath , string bifilePath)
        {
            FileStream dwFiles = new FileStream(dwfilePath, System.IO.FileMode.Open, System.IO.FileAccess.Read);
            FileStream biFiles = new FileStream(bifilePath, System.IO.FileMode.Open, System.IO.FileAccess.Read);
            StreamReader dwReader = new StreamReader(dwFiles, Encoding.Default);
            StreamReader biReader = new StreamReader(biFiles, Encoding.Default);
            //記錄每次讀取的一行記錄
            dwnamedata = new List<String> { };
            dwdata = new List<String> { };
            binamedata = new List<String> { };
            bidata = new List<String> { };
            string strLine = "";
            //記錄每行記錄中的各字段內容
            string[] aryLine = null;
            string[] tableHead = null;
            //標示列數
            int columnCount = 0;
            //標示是否是讀取的第一行
            bool IsFirst = true;
            //逐行讀取CSV中的數據
            while ((strLine = dwReader.ReadLine()) != null)
            {
                if (IsFirst == true)
                {
                    tableHead = strLine.Split(',');
                    IsFirst = false;
                    columnCount = tableHead.Length;
                    string saveline = string.Empty;
                    for (int j = 0; j < columnCount; j++)
                    {
                        tableHead[j] = tableHead[j].Replace("\"", "");
                        saveline = saveline + "|" + tableHead[j].Replace("\"", "");
                    }
                    saveline = saveline.Replace("\'", "");
                    dwnamedata = saveline.Split('|').OfType<string>().ToList();
                }
                else {
                    aryLine = strLine.Split(',');
                    columnCount = aryLine.Length;
                    string saveline = string.Empty;
                    for (int j = 0; j < columnCount; j++)
                    {
                        aryLine[j] = aryLine[j].Replace("\"", "");
                        saveline = saveline + "|" + aryLine[j].Replace("\"", "");
                    }
                    saveline = "DW" + saveline.Replace("\'", "");
                    dwdata.Add(saveline);
                    //Console.WriteLine(saveline);
                }
            }
            dwReader.Close();
            dwFiles.Close();
            strLine = "";
            aryLine = null;
            tableHead = null;
            columnCount = 0;
            IsFirst = true;
            while ((strLine = biReader.ReadLine()) != null)
            {
                if (IsFirst == true)
                {
                    tableHead = strLine.Split(',');
                    IsFirst = false;
                    columnCount = tableHead.Length;
                    string saveline = string.Empty;
                    for (int j = 0; j < columnCount; j++)
                    {
                        tableHead[j] = tableHead[j].Replace("\"", "");
                        saveline = saveline + "|" + tableHead[j].Replace("\"", "");
                    }
                    saveline = saveline.Replace("\'", "");
                    binamedata = saveline.Split('|').OfType<string>().ToList();
                }
                else {
                    aryLine = strLine.Split(',');
                    columnCount = aryLine.Length;
                    string saveline = string.Empty;
                    for (int j = 0; j < columnCount; j++)
                    {
                        aryLine[j] = aryLine[j].Replace("\"", "");
                        saveline = saveline + "|" + aryLine[j].Replace("\"", "");
                    }
                    saveline = "BI" + saveline.Replace("\'", "");
                    bidata.Add(saveline);
                }
            }
            biReader.Close();
            biFiles.Close();
        }
        private static void WriteLog(String logMsg)
        {
            //Path set
            string logPath = Properties.Settings.Default.logPath;
            String logFileName = "\\" + DateTime.Now.Year.ToString() + int.Parse(DateTime.Now.Month.ToString()).ToString("00") + int.Parse(DateTime.Now.Day.ToString()).ToString("00") + ".txt";
            String nowTime = int.Parse(DateTime.Now.Hour.ToString()).ToString("00") + ":" + int.Parse(DateTime.Now.Minute.ToString()).ToString("00") + ":" + int.Parse(DateTime.Now.Second.ToString()).ToString("00");

            File.AppendAllText(logPath + logFileName,nowTime.ToString() + " " + logMsg + "\r\n");
        }
    }
}
